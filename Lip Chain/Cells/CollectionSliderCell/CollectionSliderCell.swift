//
//  CollectionSliderCell.swift
//  Lip Chain
//
//  Created by Prashant on 22/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit


protocol VideoCellSelection {
    func videoCellSelected(forIndex:IndexPath)
}


class CollectionSliderCell: UICollectionViewCell {
    
    
    @IBOutlet weak var collectionVideos: UICollectionView!
    
    
    var delegate:VideoCellSelection?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionVideos.delegate = self
        self.collectionVideos.dataSource = self
    }
    
    /*
    var videos: [Video]?
    
    let cellId = "cellId"
    
    func fetchVideos() {
        ApiService.sharedInstance.fetchVideos { (videos: [Video]) in
     
            self.videos = videos
            self.collectionView.reloadData()
     
        }
    }
    */
}
//
class VideoCell: UICollectionViewCell {
    
}


extension CollectionSliderCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "VideoCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! VideoCell
        if indexPath.item % 2 == 0 {
            //cell.backgroundColor = UIColor.gray
        }
        else {
            //cell.backgroundColor = UIColor.green
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("indexPath.row: \(indexPath.row) , section: \(indexPath.section)")
        
        if self.delegate != nil {
            self.delegate?.videoCellSelected(forIndex: indexPath) // more model to go in this as param
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let height = (frame.width - 16 - 16) * 9 / 16
        return CGSize(width: collectionView.frame.width,height:355) // height + 16 + 88
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
