//
//  LeftViewCell.swift
//  LGSideMenuControllerDemo
//

class LeftViewCell: UITableViewCell {

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    
    var menuOption:MenuOption? {
        didSet{
            self.lblMenu.text = menuOption?.name.rawValue
            if let imageName = menuOption?.imageName{
                self.imgIcon.image = UIImage(named:imageName)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        //titleLabel.alpha = highlighted ? 0.5 : 1.0
    }

}



class LeftEarningCell: UITableViewCell {
    
    @IBOutlet weak var viewback: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnLikes: UIButton!
    
    
    var menuOption:MenuOption? {
        didSet{
            
            self.lblMenu.text = menuOption?.name.rawValue
            
            if let subT = menuOption?.subTitle?.rawValue{
                self.lblSubTitle.text = subT
            }
            
            if let imageName = menuOption?.imageName{
                self.imgIcon.image = UIImage(named:imageName)
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btnLikes.layer.roundCorners(radius: self.btnLikes.bounds.height/2)
    }
    
    @IBAction func handleBtnLikes(_ sender: UIButton) {
        
    }
    
}

class LeftSettingExpandableCell: UITableViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var expandableView: UIView!
    
    
    
}

