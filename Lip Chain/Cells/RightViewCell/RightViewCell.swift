//
//  RightViewCell.swift
//  LGSideMenuControllerDemo
//

class RightViewCell: UITableViewCell {

    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgNotificationType: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.imgUser.addRoundCorners(cornerRadius: self.imgUser.bounds.size.height/2)
        
        
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
       // titleLabel.alpha = highlighted ? 0.5 : 1.0
    }
    
}
