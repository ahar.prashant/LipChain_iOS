//
//  HomeViewController.swift
//  LipChain
//
//  Created by Prashant on 20/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class HomeViewController: LipChainViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    

    //top nav
    @IBOutlet weak var viewTopNav: UIView!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    //slider
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnLatest: UIButton!
    @IBOutlet weak var btnHot: UIButton!
    @IBOutlet weak var viewSlider: UIView!
    @IBOutlet weak var constraintLeadingSlider: NSLayoutConstraint!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var collectionSlider: UICollectionView!
    
    
    
    var isUserTap:Bool = false
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    // MARK: - Handle Btn Events
    
    @IBAction func handleBtns_Notification_Menu(_ sender: UIButton) {
        if sender.tag == 1 {
            // btn notification
            
            self.toggleRightViewAnimated(sender)
        }
        else if sender.tag == 2  {
            // btn menu
            self.toggleLeftViewAnimated(sender)
        }
    }
    
    @IBAction func handleBtn_Latest_Hot(_ sender: UIButton) {
        
        
        if sender.tag == 3 {
            // btn latest
        self.manageSelectedCategory(selectedBtn: self.btnLatest, unselectedBtn: self.btnHot)
        }
        else if sender.tag == 4  {
            // btn hot
            self.manageSelectedCategory(selectedBtn: self.btnHot, unselectedBtn: self.btnLatest)
        }

        self.scrollCollectionSliderToIndex(index: sender.tag - 3)
        
        //self.animateSliderView(sender: sender)
    
    }
    
    @IBAction func handleBtnSearch(_ sender: UIButton) {
        
        self.collectionSlider.isScrollEnabled = false
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.viewSearch.isHidden = false
            
        },completion: { (done) in
            self.txtSearch.becomeFirstResponder()
        })
        
    }
    
    
    @IBAction func handleBtnCancelSearch(_ sender: Any) {
        self.collectionSlider.isScrollEnabled = true
        self.txtSearch.text = nil
        self.viewSearch.isHidden = true
        self.view.endEditing(true)
    }
    
    // MARK: - Helper Methods
    
    func scrollCollectionSliderToIndex(index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        self.collectionSlider.scrollToItem(at: indexPath , at: UICollectionViewScrollPosition.left, animated: true)
    }
    
    func animateSliderView(sender view:UIView){
        
        UIView.animate(withDuration: 0.30,
                       delay: 0,
                       usingSpringWithDamping:1,
                       initialSpringVelocity:1,
                       options: .curveEaseOut,
                       animations: {
                        self.viewSlider.frame.origin.x = view.frame.origin.x
        },
                       completion: nil)
    }
    
    
    func manageSelectedCategory(selectedBtn:UIButton, unselectedBtn:UIButton){
        selectedBtn.setTitleColor(UIColor.rgb(red: 80, green: 193, blue: 207), for: .normal)
        unselectedBtn.setTitleColor(UIColor.lightGray, for: .normal)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeViewController: VideoCellSelection {
    

    // MARK: - Video Cell delegate
    
    func videoCellSelected(forIndex: IndexPath) {
        self.performSegue(withIdentifier: Constants.SEGUE_VIDEO_PLAY, sender: nil)
    }
    
    
    
    
    
    // MARK : - Collection view delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "CollectionSliderCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CollectionSliderCell
        if indexPath.item == 0 {
            //cell.backgroundColor = UIColor.gray
        }
        else {
            //cell.backgroundColor = UIColor.green
        }
        cell.delegate = self
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        /*
        if indexPath.item == 0 {
            self.animateSliderView(sender: self.btnLatest)
        }
        else {
            self.animateSliderView(sender: self.btnHot)
        }
        */
        //print("indexPath.row: \(indexPath.row) , section: \(indexPath.section) ***************")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.height
        return CGSize(width: collectionView.frame.width,height: height)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    

//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//        for cell in self.collectionSlider.visibleCells {
//
//            if  let indexPath = self.collectionSlider.indexPath(for: cell){
//
//                if indexPath.item == 0 {
//                    self.animateSliderView(sender: self.btnLatest)
//                }
//                else {
//                    self.animateSliderView(sender: self.btnHot)
//                }
//            }
//
//
//        }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var visibleRect = CGRect()
        visibleRect.origin = self.collectionSlider.contentOffset
        visibleRect.size = self.collectionSlider.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
        let visibleIndexPath: IndexPath? = self.collectionSlider.indexPathForItem(at: visiblePoint)
        
        if  let indexPath = visibleIndexPath{
             print("Visible cell's index is : \(indexPath.row)")
            if indexPath.item == 0 {
                self.manageSelectedCategory(selectedBtn: self.btnLatest, unselectedBtn: self.btnHot)
                self.animateSliderView(sender: self.btnLatest)
            }
            else {
                self.manageSelectedCategory(selectedBtn: self.btnHot, unselectedBtn: self.btnLatest)
                self.animateSliderView(sender: self.btnHot)
            }
        }
    }
    
//    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//
//        let index = targetContentOffset.memory.x / collectionSlider.frame.width
//
//        let indexPath = NSIndexPath(forItem: Int(index), inSection: 0)
//        menuBar.collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .None)
//
//    }
}

















