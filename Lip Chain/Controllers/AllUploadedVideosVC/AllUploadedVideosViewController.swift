//
//  AllUploadedVideosViewController.swift
//  Lip Chain
//
//  Created by Prashant on 11/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class AllUploadedVideosViewController: LipChainViewController {

    
    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var collectionVideos: UICollectionView!
    @IBOutlet weak var btnAddMoreVideos: UIGradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Handle Btn Events

    @IBAction func handleBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func handleBtnMoreVideos(_ sender: UIButton) {
        
    }

}




// MARK: -

extension AllUploadedVideosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellIdentifier = Constants.CELL_MORE_VIDEO
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width - 32 - 2 ) / 3
        let height = width - 20 //width * 9 / 16
        
        return CGSize(width: width, height: height)
        
}
}
