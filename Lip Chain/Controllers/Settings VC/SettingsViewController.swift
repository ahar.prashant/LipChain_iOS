//
//  SettingsViewController.swift
//  Lip Chain
//
//  Created by Prashant on 13/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class SettingsViewController: LipChainViewController {

    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    @IBOutlet weak var scrollViewAccount: UIScrollView!
    @IBOutlet weak var scrollViewPersonal: UIScrollView!
    
    // VIEW PERSONAL
    @IBOutlet weak var viewPersonal: UIView! {
        didSet {
            viewPersonal.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        }
    }
    @IBOutlet weak var lblUpdatePersonalHeading: UILabel!
    @IBOutlet weak var lblContactHeading: UILabel!
    
    @IBOutlet weak var txtFirstName: FloatLabelTextField!
    @IBOutlet weak var txtLastName: FloatLabelTextField!
        // dob
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblDayShow: UILabel!
    @IBOutlet weak var lblMonthShow: UILabel!
    @IBOutlet weak var lblYearShow: UILabel!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    
        //gender
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var btnMale: DLRadioButton!
    @IBOutlet weak var btnFemale: DLRadioButton!
    
        // mobile email
    @IBOutlet weak var txtMobile: FloatLabelTextField!
    @IBOutlet weak var txtEmail: FloatLabelTextField!
    @IBOutlet weak var btnUpdatePersonal: UIGradientButton!
    
    //********
    
    // VIEW ACCOUNT
    @IBOutlet weak var viewAccount: UIView! {
        didSet {
            viewAccount.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        }
    }
    
    @IBOutlet weak var lblAccountHeading: UILabel!
    @IBOutlet weak var lblPasswordHeading: UILabel!
    
    @IBOutlet weak var txtEmailAccount: FloatLabelTextField!
    @IBOutlet weak var btnChangeEmailAccount: UIButton! {
        didSet {
            btnChangeEmailAccount.addRoundCorners(cornerRadius: btnChangeEmailAccount.frame.size.height/2)
        }
    }
    
    @IBOutlet weak var txtMobileAccount: FloatLabelTextField!
    @IBOutlet weak var btnChangeMobileAccount: UIButton! {
        didSet {
            btnChangeMobileAccount.addRoundCorners(cornerRadius: btnChangeMobileAccount.frame.size.height/2)
        }
    }
    
    @IBOutlet weak var lblStopPush: UILabel!
    @IBOutlet weak var switchNotification: UISwitch!
    
    @IBOutlet weak var txtCurrentPassword: FloatLabelTextField!
    @IBOutlet weak var txtNewPassword: FloatLabelTextField!
    @IBOutlet weak var txtConfirmPassword: FloatLabelTextField!
    @IBOutlet weak var btnUpdateAccount: UIGradientButton!
    
    
    //MARK: - DropDown's
    
    let dayDropDown = DropDown()
    let monthDropDown = DropDown()
    let yearDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.dayDropDown,
            self.monthDropDown,
            self.yearDropDown
        ]
    }()
    
    
    //MARK: -
    var isPersonal = true
    
    
    
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.setUpViews()
       
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Handle Btn Events
    @IBAction func handleBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func handleBtns_Day_Month_Year(_ sender: UIButton) {
        
        if sender.tag == 1 {
            //btn day
            self.dayDropDown.show()
        }
        else if sender.tag == 2 {
            //btn month
            self.monthDropDown.show()
        }
        else if sender.tag == 3 {
            //btn year
            self.yearDropDown.show()
        }
    }
    @IBAction func handleBtnMale_Female(_ sender: DLRadioButton) {
        
        if sender.tag == 1 {
            //btn male
        }
        else if sender.tag == 2 {
            //btn female
        }
    }
    
    @IBAction func handleBtnUpdate(_ sender: UIButton) {
        if sender.tag == 1 {
            //btn update - personal
            
        }
        else if sender.tag == 2 {
            //btn update - account
            
        }
    }
    
    @IBAction func handleBtnChangeAccount(_ sender: UIButton) {
        if sender.tag == 1 {
            //btn change email
            
        }
        else if sender.tag == 2 {
            //btn change mobile
            
        }
    }
    
    @IBAction func handleSwitchNotification(_ sender: UISwitch) {
    }
    
    
    // MARK: - Helper methods
    
    func setUpDropDowns(){
        
        DropDown.appearance().textColor = UIColor.lightGray
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 12)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.groupTableViewBackground
        DropDown.appearance().cellHeight = 30
        
        
        // The view to which the drop down will appear on
        dayDropDown.anchorView = self.btnDay // UIView or UIBarButtonItem
        monthDropDown.anchorView = self.btnMonth
        yearDropDown.anchorView = self.btnYear
        
        // The list of items to display. Can be changed dynamically
        dayDropDown.dataSource      = Constants.DAYS
        monthDropDown.dataSource    = Constants.MONTHS
        yearDropDown.dataSource     = Constants.YEARS
        
        // Action triggered on selection
        dayDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblDayShow.text = "\(item)"
        }
        monthDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblMonthShow.text = "\(item)"
        }
        yearDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblYearShow.text = "\(item)"
        }
        
    }
    
    func setUpViews(){
        self.scrollViewPersonal.isHidden = !isPersonal
        self.scrollViewAccount.isHidden = isPersonal
        self.lblTitle.text = isPersonal ? Constants.SETTINGS_PERSONAL : Constants.SETTINGS_ACC
        self.setUpFonts()
        self.setUpDropDowns()
        isPersonal ? viewPersonal.setShadow(toRemove: false) : viewAccount.setShadow(toRemove: false)
    }
    
    func setUpFonts(){
 
        //labels
        self.view.setFontsToRequired(fontSize: 16.0, font:Font.OPENSAN_REGULAR, to: lblTitle, lblUpdatePersonalHeading, lblContactHeading, lblAccountHeading, lblPasswordHeading)
        self.view.setFontsToRequired(fontSize: 12.0, font: Font.OPENSAN_REGULAR, to: lblDOB, lblGender, lblStopPush)
        
        //dob
        self.view.setFontsToRequired(fontSize: 12.0, font: Font.OPENSAN_REGULAR, to: lblDayShow, lblMonthShow, lblYearShow)

        // text fields
        self.view.setFontsToRequired(fontSize: 14.0, font: Font.OPENSAN_REGULAR, to: txtFirstName, txtLastName, txtMobile, txtEmail, txtEmailAccount, txtMobileAccount, txtCurrentPassword, txtNewPassword, txtConfirmPassword) // font 11.0
       
        //btns
        self.view.setFontsToRequired(fontSize: 12.0, font: Font.OPENSAN_REGULAR, to: btnMale, btnFemale)
        self.view.setFontsToRequired(fontSize: 14.0, font: Font.OPENSAN_SEMIBOLD, to: btnUpdatePersonal, btnUpdateAccount)
        self.view.setFontsToRequired(fontSize: 10.0, font: Font.OPENSAN_SEMIBOLD, to: btnChangeEmailAccount, btnChangeMobileAccount)
    }


}
