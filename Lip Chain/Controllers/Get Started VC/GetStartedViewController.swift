//
//  GetStartedViewController.swift
//  Lip Chain
//
//  Created by Prashant on 22/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class GetStartedViewController: LipChainViewController {

    @IBOutlet weak var btnGetStarted: UIButton!
    @IBOutlet weak var btnOtp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.btnGetStarted.addRoundCorners(cornerRadius: self.btnGetStarted.bounds.size.height/2)
        
        let part1 = NSAttributedString(string: "Generate OTP",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 0.68)]
        )
        let part2 = NSAttributedString(string: " for Wallet",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 0.38)]
        )
        
        let completeString = NSMutableAttributedString()
        completeString.append(part1)
        completeString.append(part2)
        
        self.btnOtp.setAttributedTitle(completeString, for: .normal)
    }

    @IBAction func handleBtnGenerateOTP(_ sender: UIButton) {
        // open otp screen
        
        let callApi = CallApi()
        
        let urlString = "http://jewelvilla.n2.iworklab.com/jewelvilla/mobileapi/customer/login/"
        
        callApi.withUrl(url: urlString, parameters: [:], delegate: self, isBackCall:true) { (response, success) in
            
            if success {
                print(response)
                
//                let value = response.obj
                
                //Utility.main.showAlert(title: nil, message:"Something bad happened", controller: self)
            }
            else {
                //Utility.main.showAlert(title: nil, message:response["response"] as! String , controller: self)
            }
            
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
