//
//  LeftViewController.swift
//  LGSideMenuControllerDemo
//


struct MenuOption {
    let name: OptionName
    let subTitle: OptionName?
    let imageName: String
    
    init(name: OptionName, subTitle:OptionName? , imageName: String) {
        self.name = name
        self.subTitle = subTitle
        self.imageName = imageName
    }
    
    init(name: OptionName, imageName: String) {
        self.init(name: name, subTitle: nil, imageName: imageName)
    }
}

enum OptionName: String {
    case MyEarnings         = "My earning"
    case MyEarningSubTitle  = "This is a sub title"     // <<------
    case NewVideo           = "Upload a new video"
    case UploadedVideos     = "Uploaded videos"
    case HelpSupport        = "Help & Support"
    case Setting            = "Setting"
    case Privacy            = "Privacy"
    case Logout             = "Logout"
}



class LeftViewController: LipChainViewController {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewback: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserSubInfo: UILabel!
    
    
    
    @IBOutlet weak var tblMenuOptions: UITableView!
    
    
//    let titlesArray = [
//        [
//         Constants.TITLE:Constants.MY_EARNINGS,
//         "img":"visit"
//        ],
//        [Constants.TITLE:Constants.UPLOAD_NEW_VIDEO, "img":"visit"],
//        [Constants.TITLE:Constants.UPLOADED_VIDEOS, "img":"visit"],
//        [Constants.TITLE:Constants.HELP_SUPPORT, "img":"visit"],
//        [Constants.TITLE:Constants.SETTING, "img":"visit"],
//        [Constants.TITLE:Constants.PRIVACY, "img":"visit"],
//        [Constants.TITLE:Constants.LOGOUT, "img":"visit"],
//
//        ]
 
    let options: [MenuOption] = {
    
        return [
            MenuOption(name: .MyEarnings, subTitle: .MyEarningSubTitle , imageName: "like"),
            MenuOption(name: .NewVideo, imageName: "visit"),
            MenuOption(name: .UploadedVideos, imageName: "visit"),
            MenuOption(name: .HelpSupport, imageName: "visit"),
            MenuOption(name: .Setting, imageName: "visit"),
            MenuOption(name: .Privacy, imageName: "visit"),
            MenuOption(name: .Logout, imageName: "visit")
        ]
    }()

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tableView.contentInset = UIEdgeInsets(top: 44.0, left: 0.0, bottom: 44.0, right: 0.0)
        self.imgUser.layer.roundCorners(radius: self.imgUser.bounds.size.width/2)
        self.btnEditProfile.layer.roundCorners(radius: Constants.CORNER_RADIUS_3)
        
        self.tblMenuOptions.rowHeight = UITableViewAutomaticDimension
        self.tblMenuOptions.estimatedRowHeight = 55.0
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
//
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
//
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    
    
    // MARK: - Handle Btn events
    
    @IBAction func handleBtnEditProfile(_ sender: UIButton) {
         //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_MY_PROFILE)
        
    }
    
    @IBAction func handleBtnImageTap(_ sender: UIButton) {
        
        
    }
    
    
    //MARK: - Helper methods
    
    func pushViewController(storyBoardId identifier:String){
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: identifier)
        navigationController.pushViewController(viewController, animated: true)
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
    
    
    func handleMenuOptionSelection(menuOption:MenuOption){
        if menuOption.name == .MyEarnings {
            //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_USER_HOME)
            //let mainViewController = sideMenuController!
            //mainViewController.hideLeftView(animated: true, completionHandler: nil)
        }
        else if menuOption.name == .NewVideo{
            //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_DRIVER_TRANSACTION_VC)
        }
        else if menuOption.name == .UploadedVideos {
        }
        else if menuOption.name == .HelpSupport {
        }
        else if menuOption.name == .Setting{
        }
            
        else if menuOption.name == .Privacy{
            //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_MY_REQUESTS)
        }
        else if menuOption.name == .Logout{
            /*
             let mainViewController = sideMenuController!
             let navigationController = mainViewController.rootViewController as! NavigationController
             
             let viewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.STORYBOARD_ID_PHONE_VC)
             navigationController.setViewControllers([viewController], animated: false)
             mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
             */
        }
    }
}



extension LeftViewController: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.options.count // self.titlesArray.count
        }
    
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            //let dict:[String:String] =  titlesArray[indexPath.row]
            
            if indexPath.row == 0 {
               let cell = tableView.dequeueReusableCell(withIdentifier: "LeftEarningCell", for: indexPath) as! LeftEarningCell
                
                //cell.lblMenu.text = dict["title"]
                //cell.imgIcon.image = UIImage(named:dict["img"]!)
                 cell.menuOption =  self.options[indexPath.row]
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL_LEFT_EXPAND, for: indexPath) as! LeftSettingExpandableCell
                //cell.stackView.arrangedSubviews[1].isHidden = true
                cell.expandableView.isHidden = true
                return cell
            }
            else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL_LEFT_VIEW, for: indexPath) as! LeftViewCell
                /*if indexPath.row % 2 == 0 {
                    cell.viewBack.backgroundColor = UIColor.green
                }
                else {
                    cell.viewBack.backgroundColor = .blue
                }*/
                cell.menuOption =  self.options[indexPath.row]
                //cell.lblMenu.text = dict["title"]
                //cell.imgIcon.image = UIImage(named:dict["img"]!)
                return cell
            }
            
            
            
        }
    
        // MARK: - UITableViewDelegate
    
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

//            if indexPath.row != 4 {
//                if indexPath.row == 0 {
//                   return  90.0
//                }
//
//                   return  55.0
//            }
//
//            return UITableViewAutomaticDimension
            
            switch indexPath.row {
            case 0: return 90.0
            case 4: return UITableViewAutomaticDimension
            default: return 55.0
            }

        }
    
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if indexPath.row == 4 {
                let cell = tableView.cellForRow(at: indexPath) as! LeftSettingExpandableCell
                //cell.stackView.arrangedSubviews[1].isHidden = false
                cell.expandableView.isHidden = !cell.expandableView.isHidden
                self.tblMenuOptions.beginUpdates()
                self.tblMenuOptions.endUpdates()
            }
            else {
            //let dict:[String:String] = titlesArray[indexPath.row]
            self.handleMenuOptionSelection(menuOption: options[indexPath.row])
            }
            /*
            if dict[Constants.TITLE] == Constants.MY_EARNINGS {
                //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_USER_HOME)
                //let mainViewController = sideMenuController!
                //mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if dict[Constants.TITLE] == Constants.UPLOAD_NEW_VIDEO{
                //self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_DRIVER_TRANSACTION_VC)
            }
            else if dict[Constants.TITLE] == Constants.UPLOADED_VIDEOS {
            }
            else if dict[Constants.TITLE] == Constants.HELP_SUPPORT {
            }
            else if dict[Constants.TITLE] == Constants.SETTING {
            }
                
            else if dict[Constants.TITLE] == Constants.PRIVACY{
                self.pushViewController(storyBoardId: Constants.STORYBOARD_ID_MY_REQUESTS)
            }
            else if dict[Constants.TITLE] == Constants.LOGOUT{
                /*
                 let mainViewController = sideMenuController!
                 let navigationController = mainViewController.rootViewController as! NavigationController
                 
                 let viewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.STORYBOARD_ID_PHONE_VC)
                 navigationController.setViewControllers([viewController], animated: false)
                 mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
                 */
            }
            */
        }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//
//    }

}


/*
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LeftViewCell

        cell.titleLabel.text = titlesArray[indexPath.row]
        cell.separatorView.isHidden = (indexPath.row <= 3 || indexPath.row == self.titlesArray.count-1)
        cell.isUserInteractionEnabled = (indexPath.row != 1 && indexPath.row != 3)

        return cell
    }

    // MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 1 || indexPath.row == 3) ? 22.0 : 44.0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainViewController = sideMenuController!

        if indexPath.row == 0 {
            if mainViewController.isLeftViewAlwaysVisibleForCurrentOrientation {
                mainViewController.showRightView(animated: true, completionHandler: nil)
            }
            else {
                mainViewController.hideLeftView(animated: true, completionHandler: {
                    mainViewController.showRightView(animated: true, completionHandler: nil)
                })
            }
        }
        else if indexPath.row == 2 {
            let navigationController = mainViewController.rootViewController as! NavigationController
            let viewController: UIViewController!

            if navigationController.viewControllers.first is ViewController {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherViewController")
            }
            else {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewController")
            }

            navigationController.setViewControllers([viewController], animated: false)

            // Rarely you can get some visual bugs when you change view hierarchy and toggle side views in the same iteration
            // You can use delay to avoid this and probably other unexpected visual bugs
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }
        else {
            let viewController = UIViewController()
            viewController.view.backgroundColor = .white
            viewController.title = "Test \(titlesArray[indexPath.row])"

            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(viewController, animated: true)

            mainViewController.hideLeftView(animated: true, completionHandler: nil)
        }
    }
*/


