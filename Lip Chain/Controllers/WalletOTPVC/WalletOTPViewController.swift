//
//  WalletOTPViewController.swift
//  Lip Chain
//
//  Created by Prashant on 11/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class WalletOTPViewController: LipChainViewController {

    
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblProvideYourFingerprint: UILabel!
    
    @IBOutlet weak var btnGenerateOTP: UIButton!
    @IBOutlet weak var viewOTPContainer: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblWalletOTP: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imgThumb.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        self.imgThumb.addborder(borderWidth: 2, withColor: .groupTableViewBackground)
        
        self.btnCancel.addRoundCorners(cornerRadius: self.btnCancel.bounds.height/2)
        self.btnCancel.setShadow(toRemove: false)
        
        let part1 = NSAttributedString(string: "Click Here",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.rgb(red: 31, green: 135, blue: 154),
                                                    NSAttributedStringKey.font : Font.OPENSAN_ITALICS_REG.withSize(12)]
        )
        let part2 = NSAttributedString(string: " to Regenerate OTP",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.rgb(red: 191, green: 191, blue: 191)
            ]
        )
        let completeString = NSMutableAttributedString()
        completeString.append(part1)
        completeString.append(part2)
        
        self.btnGenerateOTP.setAttributedTitle(completeString, for: .normal)
        
        self.viewOTPContainer.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleBtnGenerateOTP(_ sender: UIButton) {
        
        self.setViewOTP(hide: false)
    }
    
    @IBAction func handleBtnCancel(_ sender: UIButton) {
        
       self.setViewOTP(hide: true)
    }
    
    
    
    
    func setViewOTP(hide:Bool){
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.viewOTPContainer.isHidden = hide
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
