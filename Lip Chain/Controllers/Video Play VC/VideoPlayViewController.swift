//
//  VideoPlayViewController.swift
//  Lip Chain
//
//  Created by Prashant on 31/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayViewController: LipChainViewController {

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var stackSurfers: UIStackView!
    @IBOutlet weak var viewSurfers: UIView!
    @IBOutlet weak var collectionSurfers: UICollectionView!
    @IBOutlet weak var collectionProducers: UICollectionView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var constraintHeightViewVideo: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightCollectionVideos: NSLayoutConstraint!
    
    var videoPlayerView: VideoPlayerView?
    var currentTime:CMTime?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //16 x 9 is the aspect ratio of all HD videos
        let height = self.view.frame.width * 9 / 16
        self.constraintHeightViewVideo.constant = height
        
        //adjusting height for more videos section
        let items = 6
        let width = (self.view.frame.width - 32 - 2 ) / 3
        let heightCell = width - 20
        let rows = CGFloat(items / 3)
        let remainder = CGFloat(items % 3)
        self.constraintHeightCollectionVideos.constant = (rows * heightCell) + (remainder == 0 ? 0 : heightCell) + 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        /*
         if let url = NSURL(string: urlString) {
         let player = AVPlayer(URL: url)
         
         let playerLayer = AVPlayerLayer(player: player)
         self.layer.addSublayer(playerLayer)
         playerLayer.frame = self.frame
         
         player.play()
         }
         */
        
        //self.playVideoInSmallView()
        //self.playVideo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func handleBack(_ sender: UIButton) {
        
        //self.currentTime = self.videoPlayerView?.player?.currentTime()
        //self.playVideo()
        
        // testing
        self.btnBack.isSelected = !self.btnBack.isSelected
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.viewSurfers.isHidden = self.btnBack.isSelected
        }, completion: { (completedAnimation) in
            //maybe we'll do something here later...
            //UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Fade)
        })
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func playVideoInSmallView(){
        
        let height = self.view.frame.width * 9 / 16
        let videoPlayerFrame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: height)
        let videoPlayerView = VideoPlayerView(frame: videoPlayerFrame)
        self.viewVideo.addSubview(videoPlayerView)
        self.videoPlayerView = videoPlayerView
        
        //        let urlString = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"
        //        if let url = NSURL(string: urlString) {
        //            let player = AVPlayer.init(url: url as URL)
        //
        //            let playerLayer = AVPlayerLayer(player: player)
        //            self.view.layer.addSublayer(playerLayer)
        //            //let rect = CGRect(x: 0, y: 0, width: self.viewVideo.frame.width, height: self.viewVideo.frame.height)
        //            playerLayer.frame.origin = self.viewVideo.frame.origin
        //            playerLayer.frame.size.height = self.view.frame.width * 9 / 16
        //            playerLayer.frame.size.width = self.view.frame.width
        //
        //            player.play()
        //        }
    }
    
    func playVideo() {
        
        guard let url = URL(string: "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8") else {
            return//https://devimages.apple.com.edgekey.net/samplecode/avfoundationMedia/AVFoundationQueuePlayer_HLS2/master.m3u8
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        //controller.delegate = self
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
            if let time = self.currentTime {
                player.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                //player. entersFullScreenWhenPlaybackBegins = true
                self.videoPlayerView?.player?.pause()
                
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension VideoPlayViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout /*AVPlayerViewControllerDelegate*/ {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cellIdentifier = Constants.CELL_MORE_VIDEO
        if collectionView == self.collectionProducers || collectionView == self.collectionSurfers {
            cellIdentifier = Constants.CELL_SPONSOR
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = (self.view.frame.width - 32 - 2 ) / 3
        var height = width - 20 //width * 9 / 16
        
        if (collectionView == self.collectionProducers || collectionView == self.collectionSurfers) {
            width = 81
            height = 51
        }
        
        return CGSize(width: width, height: height)
        
    }
    
    
    /*
    func playerViewControllerDidEndDismissalTransition(_ playerViewController: AVPlayerViewController){
        
        //self.currentTime = playerViewController.player?.currentTime()
        //self.videoPlayerView?.player?.seek(to: self.currentTime!, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        print("call back coming \(self.currentTime!)")
        
    }
    func playerViewControllerShouldDismiss(_ playerViewController: AVPlayerViewController) -> Bool {
        
        return false
    }
    func playerViewControllerWillBeginDismissalTransition(_ playerViewController: AVPlayerViewController)  {
        
    }
    
//    func playerViewController(_ playerViewController: AVPlayerViewController,
//                              timeToSeekAfterUserNavigatedFrom oldTime: CMTime,
//                              to targetTime: CMTime) -> CMTime {
//
//    }
 */
}


class VideoPlayerView: UIView {
    
    var player: AVPlayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .black
        
        let urlString = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"
        if let url = NSURL(string: urlString) {
            player = AVPlayer(url: url as URL)
            //            player.
            let playerLayer = AVPlayerLayer(player: player)
            self.layer.addSublayer(playerLayer)
            playerLayer.frame = self.frame
            
            player?.play()
            
            //player?.
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

