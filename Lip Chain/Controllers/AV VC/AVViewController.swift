//
//  AVViewController.swift
//  Lip Chain
//
//  Created by Prashant on 02/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit
import AVKit

class AVViewController: AVPlayerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let url = URL(string: "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8") else {
            return
        }
        let player = AVPlayer(url: url)
        
        self.player = player
        player.play()
        
        self.player?.isClosedCaptionDisplayEnabled = false
        self.showsPlaybackControls = true
//        self.player.tra
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
