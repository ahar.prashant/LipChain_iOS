//
//  UploadVideoViewController.swift
//  Lip Chain
//
//  Created by Prashant on 11/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class UploadVideoViewController: LipChainViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnPickMedia: UIButton!
    @IBOutlet weak var txtViewTitle: SAMTextView!
    @IBOutlet weak var txtViewDescription: SAMTextView!
    @IBOutlet weak var viewTags: UIView!
    @IBOutlet weak var txtTags: UITextField!
    @IBOutlet weak var constraintHeightTxtViewDescription: NSLayoutConstraint!
    
    @IBOutlet weak var viewVideoTime: UIView!
    @IBOutlet weak var lblVideoTime: UILabel!
    @IBOutlet weak var lblCountTitle: UILabel! {
        didSet {
            lblCountTitle.text = "0 / \(Limits.TITLE)"
        }
    }
    
    @IBOutlet weak var lblCountDescription: UILabel! {
        didSet {
            lblCountDescription.text = "0 / \(Limits.DESCRIPTION)"
        }
    }
    @IBOutlet weak var lblCountTags: UILabel! {
        didSet {
            lblCountTags.text = "0 / \(Limits.TAGS)"
        }
    }
    @IBOutlet weak var constraintHeightTxtTitle: NSLayoutConstraint!
    @IBOutlet weak var btnUpload: UIButton!
    //let attachmentHandler:AttachmentHandler = AttachmentHandler.shared
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // closure for image/video
        self.setAttachmentCallBacks()
        
        self.setUp()
        self.hideKeyboardEnabled()
    }
    
    deinit {
        self.imgVideo.image = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func handleBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func handleBtnPickMedia(_ sender: UIButton) {
         AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
    }
    
    @IBAction func handleBtnUpload(_ sender: UIButton) {
        
    }
    
    // MARK: - Helper methods
    fileprivate func setAttachmentCallBacks() {
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            // get your image here
            self.viewVideoTime.isHidden = true
            self.imgVideo.contentMode = .scaleAspectFill
            self.imgVideo.image = image
        }
        
        AttachmentHandler.shared.videoPickedBlock = {(url, time, thumbnail) in
            // get your compressed video url here
            self.viewVideoTime.isHidden = false
            
            var hours = "\(time.0)", minutes = "\(time.1)", sec = "\(time.2)"
            if time.0 < 10 {
                hours = "0\(hours)"
            }
            if time.1 < 10 {
                minutes = "0\(minutes)"
            }
            if time.2 < 10 {
                sec = "0\(sec)"
            }
            
            if let _ = thumbnail {
                self.imgVideo.contentMode = .scaleAspectFill
                self.imgVideo.image = thumbnail
            }
            self.lblVideoTime.text = " \(hours):\(minutes):\(sec) "
            print(time)
        }
        
        /*
        AttachmentHandler.shared.filePickedBlock = {(filePath) in
            // get your file path url here
        }
         */
    }
    
    fileprivate func setUp() {
        
        // txtview Description
        self.txtViewDescription.placeholder = "Latest trends in technology..."
        self.txtViewDescription.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtViewDescription.isScrollEnabled = false;
        self.txtViewDescription.delegate?.textViewDidChange!(self.txtViewDescription)
        self.txtViewDescription.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        
        // txtview title
        self.txtViewTitle.placeholder = "Technology era..."
        self.txtViewTitle.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtViewTitle.isScrollEnabled = false;
        self.txtViewTitle.delegate?.textViewDidChange!(self.txtViewTitle)
        self.txtViewTitle.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        
        // image/video
        self.imgVideo.addborder(borderWidth: 1, withColor: UIColor.groupTableViewBackground)
        self.imgVideo.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        
        // tags
        self.viewTags.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        self.txtTags.font = Font.OPENSAN_REGULAR.withSize(14)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    private struct Limits {
        static let TAGS = 60
        static let TITLE = 80
        static let DESCRIPTION = 150
    }

}


 extension UploadVideoViewController: UITextViewDelegate, UITextFieldDelegate {
  
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == self.txtViewDescription {
            let size = CGSize(width: self.view.frame.size.width - 32 , height: .infinity)
            let estimatedSize = textView.sizeThatFits(size)
            
            self.constraintHeightTxtViewDescription.constant = estimatedSize.height
        }
        else if textView == self.txtViewTitle {
            let size = CGSize(width: self.view.frame.size.width - 32 , height: .infinity)
            let estimatedSize = textView.sizeThatFits(size)
            
            self.constraintHeightTxtTitle.constant = estimatedSize.height
        }
       
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if text == "\n" {
            return false
        }
        
        if textView == self.txtViewDescription  {
            let newLength = textView.text.count + (text.count - range.length)
            if newLength <= Limits.DESCRIPTION {
                self.lblCountDescription.text = "\(newLength) / \(Limits.DESCRIPTION)"
                //print("************ \(newLength)")
            }
            return newLength <= Limits.DESCRIPTION
        }
        else if textView == self.txtViewTitle  {
            let newLength = textView.text.count + (text.count - range.length)
            if newLength <= Limits.TITLE {
                self.lblCountTitle.text = "\(newLength) / \(Limits.TITLE)"
                //print("************ \(newLength)")
            }
            return newLength <= Limits.TITLE
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        if newLength <= Limits.TAGS {
            self.lblCountTags.text = "\(newLength) / \(Limits.TAGS)"
            print("************ \(newLength)")
        }
        return newLength <= Limits.TAGS
        
        /*
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count
            - range.length
        return newLength <= yourTextLimit
 
         */
    }
    
}






















