//
//  LipChainViewController.swift
//  LipChain
//
//  Created by Prashant on 20/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class LipChainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addStatusBarBackgroundView(viewController: self, height: 50);
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let mainViewController = sideMenuController  {
            mainViewController.isLeftViewSwipeGestureEnabled = false
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.interactivePopGestureRecognizer?.isEnabled = false
            
            mainViewController.isLeftViewSwipeGestureEnabled = true
        }
        
    }
    
    
    func setLeftMenuGesture(toEnable enable:Bool){
        if let mainViewController = sideMenuController  {
            mainViewController.isLeftViewSwipeGestureEnabled = enable
        }
    }

}
