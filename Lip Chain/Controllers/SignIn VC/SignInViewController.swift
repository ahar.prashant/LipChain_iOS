//
//  SignInViewController.swift
//  Lip Chain
//
//  Created by Prashant on 21/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class SignInViewController: LipChainViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var btnNewAccount: UIButton!
    
    
    
    var activeTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.viewUsername.addborder(borderWidth: 1.0, withColor: UIColor.groupTableViewBackground)
        self.viewPassword.addborder(borderWidth: 1.0, withColor: UIColor.groupTableViewBackground)
        self.viewUsername.addRoundCorners(cornerRadius: self.viewUsername.bounds.size.height/2)
        self.viewPassword.addRoundCorners(cornerRadius: self.viewPassword.bounds.size.height/2)
        
        self.btnLogin.addRoundCorners(cornerRadius: self.btnLogin.bounds.size.height/2)
        
        let part1 = NSAttributedString(string: "Create a ",
                                                           attributes: [NSAttributedStringKey.foregroundColor : UIColor.lightGray]
        )
        let part2 = NSAttributedString(string: "new account",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.rgb(red: 31, green: 135, blue: 154)]
        )
        let part3 = NSAttributedString(string: " on Lipchain",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.lightGray]
        )
        
        let completeString = NSMutableAttributedString()
        completeString.append(part1)
        completeString.append(part2)
        completeString.append(part3)
        
        self.btnNewAccount.setAttributedTitle(completeString, for: .normal)
        
        self.hideKeyboardEnabled()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //self.registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //self.deRegisterKeyboardNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Handle Btn Events
    
    @IBAction func handleBtnLogin(_ sender: Any) {
    }
    
    @IBAction func handleForgotPassword(_ sender: Any) {
    }
    @IBAction func handleBtnNewAccount(_ sender: Any) {
    }
    
    
    // MARK: - Helper functions
    /*
    fileprivate func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func deRegisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidHide, object: nil)
    }
    
    @objc fileprivate func keyboardWillShow(notification: NSNotification){
        if let activeTextField = activeTextField { // this method will get called even if a system generated alert with keyboard appears over the current VC.
            let info: NSDictionary = notification.userInfo! as NSDictionary
            let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
            let keyboardSize: CGSize = value.cgRectValue.size
            
            let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
            
            // If active text field is hidden by keyboard, scroll it so it's visible
            // Your app might not need or want this behavior.
            
            var aRect: CGRect = self.view.frame
            aRect.size.height -= keyboardSize.height
            let activeTextFieldRect: CGRect? = activeTextField.superview?.frame
            let pointToCheck: CGPoint? = activeTextFieldRect?.origin
            //pointToCheck?.y += 10
            if (!aRect.contains(pointToCheck!)) {
                //scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
                scrollView.setContentOffset(CGPoint(x: 0, y: activeTextField.frame.origin.y - keyboardSize.height + 30), animated: true)
            }
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification: NSNotification){
        let contentInsets: UIEdgeInsets = .zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 
extension SignInViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
//        lastOffset = self.scrollView.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtUsername {
            txtPassword.becomeFirstResponder()
        }
        else {
            txtPassword.resignFirstResponder()
            
        }
        
        return true
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        //scrollView.isScrollEnabled = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
        //scrollView.isScrollEnabled = false
    }

}
