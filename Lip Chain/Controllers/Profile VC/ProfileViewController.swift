//
//  ProfileViewController.swift
//  Lip Chain
//
//  Created by Prashant on 29/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class ProfileViewController: LipChainViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    // view my profile
    @IBOutlet weak var viewMyProfile: UIView!
    @IBOutlet weak var imgTop_My: UIImageView!
    @IBOutlet weak var imgUser_My: UIImageView!
    @IBOutlet weak var lblUserName_My: UILabel!
    @IBOutlet weak var btnEditProfile_My: UIButton!
    @IBOutlet weak var viewDetails_My: UIView!
    @IBOutlet weak var lblName_My: UILabel!
    @IBOutlet weak var lblNameDisplay_My: UILabel!
    @IBOutlet weak var lblAge_my: UILabel!
    @IBOutlet weak var lblAgeDisplay_My: UILabel!
    @IBOutlet weak var lblDOB_My: UILabel!
    @IBOutlet weak var lblDOBDisplay_My: UILabel!
    @IBOutlet weak var lblLocation_My: UILabel!
    @IBOutlet weak var lblLocationDisplay_My: UILabel!
    @IBOutlet weak var lblSurfing_My: UILabel!
    @IBOutlet weak var lblSurfingDisplay_My: UILabel!
    @IBOutlet weak var lblInterest_My: UILabel!
    @IBOutlet weak var lblInterestDisplay_My: UILabel!
    
    // view uploaded videos
    @IBOutlet weak var viewUploadedVideos: UIView!
    @IBOutlet weak var lblUploadedVid_My: UILabel!
    @IBOutlet weak var btnAllVideos_My: UIButton!
        // main image
    @IBOutlet weak var imgMainVideo_My: UIImageView!
    @IBOutlet weak var lblMainImgDate: UILabel!
    @IBOutlet weak var lblMainImgLikes_Comments: UILabel!
    
        // small images
    @IBOutlet weak var imgSmallFirst: UIImageView!
    @IBOutlet weak var imgSmallSecond: UIImageView!
    @IBOutlet weak var imgSmallThird: UIImageView!
    
    
    
    // view Other Profile
    @IBOutlet weak var viewOtherProfile: UIView!
    @IBOutlet weak var imgUser_Other: UIImageView!
    @IBOutlet weak var lblUser_Other: UILabel!
    @IBOutlet weak var viewFollow_Other: UIView!
    @IBOutlet weak var lblUnfollow_Other: UILabel!
    @IBOutlet weak var lblFollowersCount_Other: UILabel!
    
    @IBOutlet weak var btnLike_Other: UIButton!
    @IBOutlet weak var btnUnfollow_Other: UIButton!
    @IBOutlet weak var viewDetails_Other: UIView!
    
    @IBOutlet weak var lblName_Others: UILabel!
    @IBOutlet weak var lblNameDisplay_Others: UILabel!
    @IBOutlet weak var lblAge_Others: UILabel!
    @IBOutlet weak var lblAgeDisplay_Others: UILabel!
    @IBOutlet weak var lblDOB_Others: UILabel!
    @IBOutlet weak var lblDOBDisplay_Others: UILabel!
    @IBOutlet weak var lblLocation_Others: UILabel!
    @IBOutlet weak var lblLocationDisplay_Others: UILabel!
    @IBOutlet weak var lblSurfing_Others: UILabel!
    @IBOutlet weak var lblSurfingDisplay_Others: UILabel!
    @IBOutlet weak var lblViewersRate_Others: UILabel!
    @IBOutlet weak var viewRating_Others: HCSStarRatingView!
    @IBOutlet weak var lblVideosUploaded_Others: UILabel!
    @IBOutlet weak var lblVideoUploadedDisplay_Others: UILabel!
    
    
    
    var isMyProfile:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewMyProfile.isHidden = self.isMyProfile ? false : true
        self.viewOtherProfile.isHidden = self.isMyProfile ? true : false
        
        isMyProfile ? self.setUpViewMyProfile() : self.setUpViewOthersProfile()
        
        self.lblTitle.text = isMyProfile ? "My Profile" : "Profile"
        self.viewRating_Others.value = 4.0 // TODO: - Change this value from api-----------
    
        
        self.setFonts()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Handle Btns Event
    @IBAction func handleBtnBack(_ sender: UIButton) {
        
    }
    
    @IBAction func handleBtnEditProfile(_ sender: UIButton) {
    }
    
    @IBAction func handleBtnAllVideos(_ sender: UIButton) {
    }
    
    @IBAction func handleBtnsVideos_Images(_ sender: UIButton) {
        if sender.tag == 1{
            //btn main image
        }
        else if sender.tag == 2{
            // btn small 1st image
        }
        else if sender.tag == 3{
            // btn small 2nd image
        }
        else if sender.tag == 4{
            // btn small 3rd image
        }
        
    }
    
    @IBAction func handleBtnUnfollow_Like(_ sender: UIButton) {
        
        if sender.tag == 1{
            // btn Unfollow
        }
        else if sender.tag == 2 {
            //btn like
        }
    }
    
    
    
    // MARK: - Helper functions
    
    private func setFonts(){
        
        self.btnEditProfile_My.titleLabel?.font = Font.OPENSAN_SEMIBOLD.withSize(10.0)
        self.btnAllVideos_My.titleLabel?.font = Font.OPENSAN_REGULAR.withSize(9.0)
        self.lblUploadedVid_My.font = Font.OPENSAN_REGULAR.withSize(15.0)
        
        self.lblUnfollow_Other.font = Font.OPENSAN_SEMIBOLD.withSize(12.0)
        self.lblFollowersCount_Other.font = Font.OPENSAN_REGULAR.withSize(8.0)
        self.btnLike_Other.titleLabel?.font = Font.OPENSAN_REGULAR.withSize(9.0)
        
        self.view.setFontToLabels(fontSize: 14.0, font:Font.OPENSAN_SEMIBOLD, to: lblUserName_My,lblUser_Other)
        
        self.view.setFontToLabels(fontSize: 14.0, font:Font.OPENSAN_REGULAR, to: lblLocation_My, lblSurfing_My, lblInterest_My, lblDOB_My, lblName_My, lblAge_my,lblName_Others, lblAge_Others, lblDOB_Others, lblSurfing_Others, lblLocation_Others, lblVideosUploaded_Others, lblViewersRate_Others)
        
        self.view.setFontToLabels(fontSize: 11.0, font:Font.OPENSAN_REGULAR, to: lblAgeDisplay_My, lblDOBDisplay_My, lblNameDisplay_My, lblSurfingDisplay_My, lblInterestDisplay_My, lblLocationDisplay_My, lblAgeDisplay_Others, lblDOBDisplay_Others, lblNameDisplay_Others, lblSurfingDisplay_Others, lblLocationDisplay_Others, lblVideoUploadedDisplay_Others)
    }
    
    
    private func setUpViewMyProfile(){
        self.imgUser_My.addRoundCorners(cornerRadius: self.imgUser_My.bounds.height/2)
        self.imgUser_My.addborder(borderWidth: 2.0, withColor: nil)
        
        self.viewDetails_My.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        self.viewUploadedVideos.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
    }
    
    private func setUpViewOthersProfile(){
        self.imgUser_Other.addRoundCorners(cornerRadius: self.imgUser_Other.bounds.height/2)
        self.imgUser_Other.addborder(borderWidth: 2.0, withColor: nil)
        
        self.viewDetails_Other.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        self.viewFollow_Other.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
