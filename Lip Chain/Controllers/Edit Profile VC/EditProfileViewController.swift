//
//  EditProfileViewController.swift
//  Lip Chain
//
//  Created by Prashant on 13/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class EditProfileViewController: LipChainViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewProfile: UIView! {
        didSet{
            viewProfile.addRoundCorners(cornerRadius: Constants.CORNER_RADIUS_3)
        }
    }
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnUserImage: UIButton!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var txtAge: UITextField!
    
    @IBOutlet weak var lblDOB: UILabel!
    
    
    @IBOutlet weak var lblDayShow: UILabel!
    @IBOutlet weak var lblMonthShow: UILabel!
    @IBOutlet weak var lblYearShow: UILabel!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    
    
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var lblSurfing: UILabel!
    @IBOutlet weak var txtSurfing: UITextField!
    
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var txtInterests: UITextField!
    
    @IBOutlet weak var lblAwards: UILabel!
    @IBOutlet weak var txtAwards: UITextField!
    
    @IBOutlet weak var btnUpdate: UIGradientButton! {
        didSet{
            btnUpdate.titleLabel?.font = Font.OPENSAN_SEMIBOLD.withSize(14)
        }
    }
    
    //MARK: - DropDown's
    
    let dayDropDown = DropDown()
    let monthDropDown = DropDown()
    let yearDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.dayDropDown,
            self.monthDropDown,
            self.yearDropDown
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpFonts()
        self.setUpDropDowns()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Handle Btns Event
    @IBAction func handleBtnBack(_ sender: UIButton) {
        
    }
    
    @IBAction func handleBtnChangeImage(_ sender: UIButton) {
        
    }
    
    @IBAction func handleBtns_Day_Month_Year(_ sender: UIButton) {
        
        if sender.tag == 1 {
            //btn day
            self.dayDropDown.show()
        }
        else if sender.tag == 2 {
            //btn month
            self.monthDropDown.show()
        }
        else if sender.tag == 3 {
            //btn year
            self.yearDropDown.show()
        }
    }
    
    
    @IBAction func handleBtnUpdate(_ sender: UIButton) {
        
    }
    
    // MARK: - Helper functions
    
    func setUpDropDowns(){
        
        DropDown.appearance().textColor = UIColor.lightGray
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 12)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.groupTableViewBackground
        DropDown.appearance().cellHeight = 30
        
        
        // The view to which the drop down will appear on
        dayDropDown.anchorView = self.btnDay // UIView or UIBarButtonItem
        monthDropDown.anchorView = self.btnMonth
        yearDropDown.anchorView = self.btnYear
        
        // The list of items to display. Can be changed dynamically
        dayDropDown.dataSource = ["1","2","3","4","5","6","7","8","9","10",
                                  "11","12","13","14","15","16","17","18","19","20",
                                  "21","22","23","24","25","26","27","28","29","30","31"
        ]
        
        monthDropDown.dataSource = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"]
        
        yearDropDown.dataSource = ["1960",
                                   "1961",
                                   "1962",
                                   "1963",
                                   "1964",
                                   "1965",
                                   "1966",
                                   "1967",
                                   "1968",
                                   "1969",
                                   "1970",
                                   "1971",
                                   "1972",
                                   "1973",
                                   "1974",
                                   "1975",
                                   "1976",
                                   "1977",
                                   "1978",
                                   "1979",
                                   "1980",
                                   "1981",
                                   "1982",
                                   "1983",
                                   "1984",
                                   "1985",
                                   "1986",
                                   "1987",
                                   "1988",
                                   "1989",
                                   "1990",
                                   "1991",
                                   "1992",
                                   "1993",
                                   "1994",
                                   "1995",
                                   "1996",
                                   "1997",
                                   "1998",
                                   "1999",
                                   "2000",
                                   "2001",
                                   "2002",
                                   "2003",
                                   "2004",
                                   "2005",
                                   "2006",
                                   "2007",
                                   "2008",
                                   "2009",
                                   "2010",
                                   "2011",
                                   "2012",
                                   "2013",
                                   "2014",
                                   "2015",
                                   "2016",
                                   "2017",
                                   "2018",]
        
        // Action triggered on selection
        dayDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblDayShow.text = "\(item)"
        }
        monthDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblMonthShow.text = "\(item)"
        }
        yearDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblYearShow.text = "\(item)"
        }
        
    }
    
    
    func setUpFonts(){
        
        self.lblTitle.font = Font.OPENSAN_REGULAR.withSize(16)
        self.lblUserName.font = Font.OPENSAN_SEMIBOLD.withSize(14)
        //name
        self.lblName.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtName.font = Font.OPENSAN_REGULAR.withSize(14)
        ////age
        self.lblAge.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtAge.font = Font.OPENSAN_REGULAR.withSize(14)
        //dob
        self.lblDOB.font = Font.OPENSAN_REGULAR.withSize(14)
        self.lblDayShow.font = Font.OPENSAN_REGULAR.withSize(12)
        self.lblMonthShow.font = Font.OPENSAN_REGULAR.withSize(12)
        self.lblYearShow.font = Font.OPENSAN_REGULAR.withSize(12)
        // location
        self.lblLocation.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtLocation.font = Font.OPENSAN_REGULAR.withSize(14)
        // surfing
        self.lblSurfing.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtSurfing.font = Font.OPENSAN_REGULAR.withSize(14)
        //interset
        self.lblInterest.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtInterests.font = Font.OPENSAN_REGULAR.withSize(14)
        //awards
        self.lblAwards.font = Font.OPENSAN_REGULAR.withSize(14)
        self.txtAwards.font = Font.OPENSAN_REGULAR.withSize(14)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
