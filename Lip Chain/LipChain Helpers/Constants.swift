//
//  Constants.swift
//  RoverTaxiUser
//
//  Created by Prashant on 08/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import Foundation
import UIKit


struct Constants {
    
    static let SCREEN_WIDTH  = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
    static let CORNER_RADIUS_3:CGFloat = 3.0
    static let CORNER_RADIUS_5:CGFloat = 5.0

    
    // CELL ID'S
    static let CELL_MORE_VIDEO  = "MoreVideo"
    static let CELL_SPONSOR     = "SponsorCell"
    static let CELL_RIGHT_VIEW  = "RightViewCell"
    static let CELL_LEFT_VIEW   = "LeftViewCell"
    static let CELL_LEFT_EXPAND = "LeftSettingExpandableCell"
    
    
    // SEGUES
    static let SEGUE_VIDEO_PLAY     = "VideoPlaySegue"
    
    static let SEGUE_USER_HOME      = "UserHomeSegue"
    static let SEGUE_SIGNUP         = "SignUpSegue"
    static let SEGUE_TRIP_COMPLETE  = "TripCompleteSegue"
    static let SEGUE_DRIVER_HOME    = "DriverHomeSegue"
    static let SEGUE_DRIVER_NOTIF   = "NotificationsSegue"
    static let SEGUE_DRIVER_TRIP   = "TripViewSegue"
    static let UNWIND_SEGUE_DRIVER_HOME = "DriverHomeUnwindSegue"
    
    
    
    
    // STRINGS
    
    static let ACCEPT       = "ACCEPT"
    static let REJECT       = "REJECT"
    static let REACHED      = "REACHED"
    static let CANCEL       = "CANCEL"
    static let START_TRIP   = "START TRIP"
    static let NO_INTERNET  = "Please check your internet connection."
    
    static let SETTINGS_ACC      = "Settings - Account"
    static let SETTINGS_PERSONAL = "Settings - Personal"
    
    //trip status
    static let TRIP_STATUS_BOOKED   = "Booked"
    static let TRIP_STATUS_ONGOING  = "Ongoing"
    //static let TRIP_STATUS_BOOKED = "Booked"
    
    //side menu
    static let MY_EARNINGS = "My earning"
    static let UPLOAD_NEW_VIDEO = "Upload a new video"
    static let UPLOADED_VIDEOS = "Uploaded videos"
    static let HELP_SUPPORT = "Help & Support"
    static let LOGOUT = "Logout"
    static let TITLE        = "title"
    static let SETTING = "Setting"
    static let PRIVACY = "Privacy"
    
    //storyboard id's
    static let STORYBOARD_ID_USER_HOME      = "HomeViewController"
    static let STORYBOARD_ID_MY_REQUESTS    = "MyRequestsViewController"
    static let STORYBOARD_ID_MY_FAVORITES   = "FavouritesViewController"
    static let STORYBOARD_ID_CREDIT_BAL     = "CreditBalanceViewController"
    static let STORYBOARD_ID_MY_PROFILE     = "MyProfileViewController"
    static let STORYBOARD_ID_PHONE_VC       = "PhoneViewController"
    static let STORYBOARD_ID_DRIVER_TRANSACTION_VC = "DriverTransactionsViewController"
    
    
    
    
    static let DAYS = ["1","2","3","4","5","6","7","8","9","10",
                         "11","12","13","14","15","16","17","18","19","20",
                         "21","22","23","24","25","26","27","28","29","30","31"]
    
    static let MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"]
    
    static let YEARS = ["1960",
                              "1961",
                              "1962",
                              "1963",
                              "1964",
                              "1965",
                              "1966",
                              "1967",
                              "1968",
                              "1969",
                              "1970",
                              "1971",
                              "1972",
                              "1973",
                              "1974",
                              "1975",
                              "1976",
                              "1977",
                              "1978",
                              "1979",
                              "1980",
                              "1981",
                              "1982",
                              "1983",
                              "1984",
                              "1985",
                              "1986",
                              "1987",
                              "1988",
                              "1989",
                              "1990",
                              "1991",
                              "1992",
                              "1993",
                              "1994",
                              "1995",
                              "1996",
                              "1997",
                              "1998",
                              "1999",
                              "2000",
                              "2001",
                              "2002",
                              "2003",
                              "2004",
                              "2005",
                              "2006",
                              "2007",
                              "2008",
                              "2009",
                              "2010",
                              "2011",
                              "2012",
                              "2013",
                              "2014",
                              "2015",
                              "2016",
                              "2017",
                              "2018",]
}


/*
 public class func openSansFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans", size: makeSize(size))
 }
 
 public class func openSansBoldFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-Bold", size: makeSize(size))
 }
 
 public class func openSansBoldItalicFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-BoldItalic", size: makeSize(size))
 }
 
 public class func openSansExtraBoldFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-Extrabold", size: makeSize(size))
 }
 
 public class func openSansExtraBoldItalicFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-ExtraboldItalic", size: makeSize(size))
 }
 
 public class func openSansItalicFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-Italic", size: makeSize(size))
 }
 
 public class func openSansLightFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-Light", size: makeSize(size))
 }
 
 public class func openSansLightItalicFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSansLight-Italic", size: makeSize(size))
 }
 
 public class func openSansSemiboldFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-Semibold", size: makeSize(size))
 }
 
 public class func openSansSemiboldItalicFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans-SemiboldItalic", size: makeSize(size))
 }

 */

// MARK: - Font

struct Font {
    
    //let font = Font.header.withSize(18.0)
    
    
    static let OPENSAN_REGULAR  = UIFont(name: "OpenSans", size: 14.0)!
    static let OPENSAN_LIGHT    = UIFont(name: "OpenSans-Light", size: 14.0)!
    static let OPENSAN_BOLD     = UIFont(name: "OpenSans-Bold", size: 14.0)!
    static let OPENSAN_SEMIBOLD = UIFont(name: "OpenSans-Semibold", size: 14.0)!
    static let OPENSAN_ITALICS_REG = UIFont(name: "OpenSans-Italic", size: 14.0)!
    static let OPENSAN_ITALICS_SEMI = UIFont(name: "OpenSans-SemiboldItalic", size: 14.0)!
    
    static func ROBOTO_REGULAR(size: CGFloat) -> UIFont {
        return UIFont(name: "Regular", size: size)!
    }
    
    /*
      OpenSans-Bold
      OpenSans-Light
      OpenSans-Semibold
      OpenSans
      OpenSans-SemiboldItalic
      OpenSans-Italic
     */
    
}



struct BuildType {
    
    static var IS_DRIVER:Bool {
        
        #if DRIVER
            return true
        //let API_PATH = "https://devlopement.abc.com/v1/"
        #else
            return false
        //let API_PATH = "https://production.abc.com/v1/"
        #endif
    }
}
