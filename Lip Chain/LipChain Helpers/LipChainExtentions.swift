//
//  RoverExtentions.swift
//  RoverTaxiUser
//
//  Created by Prashant on 08/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import Foundation
import UIKit


//extension UIView {
//
//    func setDesiredCornerRadius(radius:CGFloat) {
//
//    }
//
//}

extension CALayer {
    func addShadow() {
        self.shadowOffset = .zero
        self.shadowOpacity = 0.2
        self.shadowRadius = 5
        self.shadowColor = UIColor.black.cgColor
        self.masksToBounds = false
    }

    
    func roundCorners(radius: CGFloat) {
        self.cornerRadius = radius
    }
}


extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib11() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func setShadow(withCorners radius:CGFloat){
        self.layer.addShadow()
        self.layer.roundCorners(radius: radius)
    }
    
    func setShadow(toRemove bool:Bool){
        if bool {
            self.layer.shadowOpacity = 0.0
        }
        else {
            self.layer.addShadow()
        }
    }
    
    func setThinShadow(){
        /*
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 2.0
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        */
        //CGSize(width: 0, height: 1.0)
        self.setThinShadowForSize(size: CGSize(width: 1, height: 1))
    }
    func setThinShadowForSize(size:CGSize?){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = size ?? CGSize(width: -1, height: 0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        //self.layer.shouldRasterize = true // --> ui gets blurry
    }
    
    func addborder(borderWidth width:CGFloat, withColor color:UIColor?){
        self.layer.borderWidth = width
        if let c = color {
            self.layer.borderColor = c.cgColor
        }
        else {
            self.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    func addRoundCorners(cornerRadius radius: CGFloat){
        self.layer.roundCorners(radius: radius)
    }
    
    
    func setFontToLabels(fontSize:CGFloat, font:UIFont, to labels: UILabel...) {
        for lbl in labels {
            lbl.font = font.withSize(fontSize)
        }
        
    }
    
    func setFontsToRequired(fontSize:CGFloat, font:UIFont, to views: UIView...) {
        
        for view in views {
            if let label = view as? UILabel {
                label.font = font.withSize(fontSize)
            }
            else if let textField = view as? UITextField {
                textField.font = font.withSize(fontSize)
            }
            else if let textView = view as? UITextView {
                textView.font = font.withSize(fontSize)
            }
            else if let btn = view as? UIButton {
                btn.titleLabel?.font = font.withSize(fontSize)
            }
        }
    }
    
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}


extension UITextField {
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}



extension UIViewController {
    
    /**
     Add a status bar background
     */
    func addStatusBarBackgroundView(viewController: UIViewController, height:CGFloat) -> Void {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size:CGSize(width: Constants.SCREEN_WIDTH, height:height))
        let view : UIView = UIView.init(frame: rect)
        view.backgroundColor = UIColor.rgb(red: 86, green: 210, blue: 214)// UIColor.init(red: 76/255, green: 177/255, blue: 190/255, alpha: 1) //Replace value with your required background color
        viewController.view?.addSubview(view)
        viewController.view?.sendSubview(toBack: view)  //Important
    }
    
    /**
      Enable dismiss of keyboard when tap outside
     */
    func hideKeyboardEnabled(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        //tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard(){
        //print("****")
        self.view.endEditing(true)
    }
    
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
