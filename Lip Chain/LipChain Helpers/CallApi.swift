//
//  CallApi.swift
//  Lip Chain
//
//  Created by Prashant on 10/09/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import Foundation
//import Reachability
import Alamofire

typealias completionHandler = (_ result:Dictionary<String, Any>, _ success:Bool) -> Void

class CallApi {
    
    
    class var sharedInstance:CallApi
    {
        struct Singleton {
            static let instance = CallApi()
        }
        return Singleton.instance
    }
    
    
    /*
    func checkNetworkStatus() ->  Bool
    {
        var isConnected:Bool!
        let reachability = Reachability()!
        
        if reachability.connection == .wifi || reachability.connection == .cellular
        {
            print("connection available")
            isConnected = true
        }
        else if reachability.isReachable == false
        {
            print("connection lost")
            isConnected = false
        }
        else
        {
            print("No network Connecion")
            isConnected = false
        }
        return isConnected
    }
    */
    //typealias CompletionHandler = (_ response:Dictionary<String, Any>,_ success:Bool) -> Void
    
    func withUrl(url:String, parameters:Dictionary<String,Any>, delegate:UIViewController, completion: @escaping completionHandler) {
        
        withUrl(url:url, parameters:parameters, delegate:delegate, isBackCall:false, completion:completion)
    }
    
    func withUrl(url:String, parameters:Dictionary<String,Any>, delegate:UIViewController, isBackCall:Bool, completion: @escaping completionHandler) {
        
        var request = URLRequest(url:URL(string:url)!)
        request.httpMethod = "POST"
        
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("X-HTTP-Method-Override", forHTTPHeaderField: "Accept-Encoding")
        request.timeoutInterval = 300
        
        Alamofire.request(request).responseJSON { response in
                
                switch response.result {    // response serialization result
                    
                case .failure(let error):
                    
                    print(error.localizedDescription)
                    let  dict:[String:AnyObject] = Dictionary()
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                    }
                    completion(dict ,false)
                    
                case .success(let responseObject):
                    //print(responseObject)
                    
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                    }
                    /*
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)") // original server data as UTF8 string
                    }
                    */
                    if !isBackCall {
                        //to show any error msg if any
                        self.validateResponse(response: responseObject as! Dictionary, delegate: delegate)
                    }
                    
                    //Utility.main.showAlert(title: nil, message:"Something bad happened", controller: delegate)
                    completion(responseObject as! Dictionary ,true)
                    
                }
        }
        
        
        
    }
    
    func validateResponse(response:Dictionary<String,Any>, delegate:UIViewController){
        
        if let description = response["description"] as? String{
            
            Utility.main.showAlert(title: nil, message:description, controller: delegate)
        }else{
            if response.isEmpty{
                Utility.main.showAlert(title: nil, message: Constants.NO_INTERNET, controller: delegate)
                
            }else{
                Utility.main.showAlert(title: nil, message: response["message"] as! String, controller: delegate)
            }
        }
    }
    
    
    
}
